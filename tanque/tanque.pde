import processing.serial.*;



import ddf.minim.*; 
Minim minim;
AudioPlayer player;

Serial miPuerto;
float dist;     //Datos recibidos por el Puerto serial
float Altura;

void setup(){
  
size(1200,700);
  
//printArray(Serial.list());
println(Serial.list());
miPuerto =new Serial(this, Serial.list()[1], 9600);
miPuerto.bufferUntil('\n');

minim=new Minim(this);
player=minim.loadFile("Buzzer_Alarm_01_Sound_Effect_Mp3_166.mp3");
}

void draw(){ 
background(136, 203, 255);
println(dist);
fill(255);
rect(700,140,400,500);

Altura=28-dist;

textSize(60);text("Control del Nivel de agua",50,100);
textSize(40);text("Estado:",50,190);
textSize(40);text("Altura:",50,290);
textSize(40);text(Altura,220,290);
textSize(20);text("Por: Matias  Martinez",50,600);
textSize(20);text(" Cristian Molina",80,635);
textSize(20);text(" Alejandro Villegas",80,670);

if(Altura<11){
  textSize(40);text("LLenando",220,190); 
  textSize(50);text("Nivel demasiado bajo(<50%)",50,450);
  player.play();
}
else{
textSize(40);text("Estable",220,190); 
player.rewind();
}

if((Altura>=0) && (Altura<=24)){
  fill(2,60,104);
rect(700,640,400,-Altura*22.5);
}
}
void serialEvent(Serial miPuerto){
  String inString=miPuerto.readStringUntil('\n');
  dist=float(inString);
}
